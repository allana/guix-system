(define-module (allana packages kubernetes)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages docker)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages node)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages version-control)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public yamlfmt
  (package
    (name "yamlfmt")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/google/yamlfmt/releases/download/v" version "/yamlfmt_0.13.0_Linux_x86_64.tar.gz"))
       (sha256 (base32 "1bp45ysc1ar560njjlxzf1dbxijh7qpzngy1qpsa9bx8akbrcgh4"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "yamlfmt" #o755))))
       #:substitutable? #f
       #:install-plan '(("yamlfmt" "/bin/"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "yamlfmt binary")
    (description "yamlfmt binary")
    (home-page "https://github.com/google/yamlfmt")
    (license license:asl2.0)))

(define-public dive
  (package
    (name "dive")
    (version "0.12.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/wagoodman/" name "/releases/download/v" version "/" name "_" version "_linux_amd64.tar.gz"))
       (sha256 (base32 "15iyxpyp5jabii5cvzygi1khshil2x3jdgsg1jamz4504djrd9r0"))))
    (build-system copy-build-system)
    (arguments
     '(#:substitutable? #f
       #:install-plan '(("dive" "/bin/"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "A tool for exploring each layer in a docker image")
    (description "A tool for exploring a docker image, layer contents, and
discovering ways to shrink the size of your Docker/OCI image.")
    (home-page "https://github.com/wagoodman/dive")
    (license license:expat)))


(define-public diffoci
  (package
   (name "diffoci")
   (version "0.1.5")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://github.com/reproducible-containers/diffoci/releases/download/v" version "/diffoci-v" version ".linux-amd64"))
     (sha256 (base32 "13v9c2wa90hdba4infjd0ff4iaiqjdak03aipnk4as8rj3k5zlh1"))))
   (build-system copy-build-system)
   (arguments
    `(#:phases (modify-phases %standard-phases
			      (add-after 'unpack 'chmod
					 (lambda* (#:key #:allow-other-keys)
					   (rename-file ,(string-append "diffoci-v" version ".linux-amd64")
							"diffoci")
					   (chmod "diffoci" #o755))))
      #:substitutable? #f
      #:install-plan '(("diffoci" "/bin/diffoci"))))
   (supported-systems '("x86_64-linux"))
   (synopsis "Diff tool for Docker and OCI container images")
   (description "diffoci compares Docker and OCI container images for helping
reproducible builds")
   (home-page "https://github.com/reproducible-containers/diffoci")
   (license license:asl2.0)))

(define-public kops
  (package
   (name "kops")
   (version "1.22.3")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/kubernetes/kops")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "1346w3hiidnk4w838ymp5lmq6v7r88h48m7bpmz3r3l8n9g41km9"))))
   (build-system go-build-system)
   (native-inputs `(("git" ,git)
                    ("gawk" ,gawk)
                    ("sed" ,sed)))
   (inputs `(("python" ,python)
             ("perl" ,perl)
             ("ruby" ,ruby)
             ("node" ,node)))
   (arguments
    '(#:import-path "k8s.io/kops/cmd/kops"
      #:unpack-path "k8s.io/kops"
      #:install-source? #f
      #:tests? #f))
   (home-page "https://kops.sigs.k8s.io/")
   (supported-systems '("x86_64-linux"))
   (synopsis "The easiest way to get a production grade Kubernetes
cluster up and running")
   (description
    "kops helps you create, destroy, upgrade and maintain
production-grade, highly available, Kubernetes clusters from the
command line. AWS (Amazon Web Services) is currently officially
supported, with GCE and OpenStack in beta support, and VMware vSphere
in alpha, and other platforms planned")
   (license license:asl2.0)))

(define-public kubectl
  (package
    (name "kubectl")
    (version "1.25.9")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://dl.k8s.io/release/v" version "/bin/linux/amd64/kubectl"))
       (sha256 (base32 "046bqy1swn3if6qcf1vj5ih58xxl2k5wrwwfi8mhswrh6qxym9da"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "kubectl" #o755))))
       #:substitutable? #f
       #:install-plan '(("kubectl" "/bin/"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "kubectl binary")
    (description "kubectl binary")
    (home-page "https://github.com/kubernetes/kubernetes")
    (license license:asl2.0)))

(define-public kustomize
  (package
   (name "kustomize")
   (version "5.5.0")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://github.com/kubernetes-sigs/" name
           "/releases/download/" name "/v" version "/" name "_v"
           version "_linux_amd64.tar.gz"))
     (sha256
      (base32 "0xrapjfqpm9yca5q3cqpxggsjx8i9ysk0h396w5wyiqc1aks60v7"))))
   (build-system copy-build-system)
   (arguments
    '(#:substitutable? #f
      #:install-plan '(("kustomize" "/bin/"))))
   (supported-systems '("x86_64-linux"))
   (synopsis "Customization of kubernetes YAML configurations")
   (description
    "kustomize lets you customize raw, template-free YAML files for
multiple purposes, leaving the original YAML untouched and usable as
is.")
   (home-page "https://kustomize.io/")
   (license license:asl2.0)))

(define-public helm
  (package
   (name "helm")
   (version "3.16.2")
   (source (origin
            (method url-fetch)
            (uri (string-append
                  "https://get.helm.sh/helm-v" version "-linux-amd64.tar.gz"))
            (sha256
             (base32
              "1fxma1pc7l7mlxqdb4yd4sd2jml11161sa9xsdh38cvyhjdkf64k"))))
   (build-system copy-build-system)
   (arguments
    '(#:phases
      (modify-phases %standard-phases
                    (replace 'unpack
                             (lambda* (#:key source #:allow-other-keys)
                               (invoke "tar" "-xvf" source)))
                    (add-before 'install 'chmod
                                (lambda _
                                  (chmod "linux-amd64/helm" #o555))))
     #:substitutable? #f
     #:install-plan '(("linux-amd64/helm" "bin/"))))
   (home-page "https://helm.sh")
   (supported-systems '("x86_64-linux"))
   (synopsis "The package manager for Kubernetes")
   (description
    "Helm helps you manage Kubernetes applications - Helm Charts help you
define, install, and upgrade Kubernetes applications.")
   (license license:asl2.0)))

(define-public terraform
  (package
   (name "terraform")
   (version "1.5.6")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://releases.hashicorp.com/" name "/" version "/" name "_" version "_linux_amd64.zip"))
     (sha256 (base32 "0iz3b2f0sin1dwn57y7r1596ic33ri30i4j1r73q5f5xxig17r9x"))))
   (build-system copy-build-system)
   (arguments
    '(#:substitutable? #f
      #:install-plan '(("terraform" "/bin/"))))
   (native-inputs (list unzip))
   (supported-systems '("x86_64-linux"))
   (synopsis "Terraform is a tool for building, changing, and versioning
infrastructure safely and efficiently.")
   (description "Terraform enables you to safely and predictably create, change, and
improve infrastructure. It is an open source tool that codifies APIs
into declarative configuration files that can be shared amongst team
members, treated as code, edited, reviewed, and versioned.")
   (home-page "https://helm.sh/")
   (license license:mpl2.0)))

(define-public argocd
  (package
    (name "argocd")
    (version "2.9.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/argoproj/argo-cd/releases/download/v" version "/argocd-linux-amd64"))
       (sha256 (base32 "0w4dibmv4p7jpw9y231gvzp96sgwqqzdh6gvakxzwmwrixaq0z9g"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "argocd-linux-amd64" #o755))))
       #:substitutable? #f
       #:install-plan '(("argocd-linux-amd64" "/bin/argocd"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "argocd binary")
    (description "argocd binary")
    (home-page "https://argo-cd.readthedocs.io")
    (license license:asl2.0)))

(define-public argo
  (package
    (name "argo")
    (version "3.4.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/argoproj/argo-workflows/releases/download/v" version "/argo-linux-amd64.gz"))
       (sha256 (base32 "1zhilmgzrknlq4nivjyvngnw8cmpryq8628abrrh2rzl6qawc9zf"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "argo-linux-amd64" #o755))))
       #:substitutable? #f
       #:install-plan '(("argo-linux-amd64" "/bin/argo"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "argo binary")
    (description "argo binary")
    (home-page "https://argoproj.github.io/argo-workflows/")
    (license license:asl2.0)))

(define-public kyverno
  (package
    (name "kyverno")
    (version "1.10.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/" name "/" name "/releases/download/v" version "/" name "-cli_v" version "_linux_x86_64.tar.gz"))
       (sha256 (base32 "0qprj6idwqpig0j484i79y3x7z1bjkxsbf2ym9cygq2p7cbyc63b"))))
    (build-system copy-build-system)
    (arguments
     '(#:substitutable? #f
       #:install-plan '(("kyverno" "/bin/"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "Kubernetes Native Policy Management")
    (description "Kyverno is a policy engine designed for Kubernetes. It
can validate, mutate, and generate configurations using admission
controls and background scans. Kyverno policies are Kubernetes
resources and do not require learning a new language. Kyverno is
designed to work nicely with tools you already use like kubectl,
kustomize, and Git.")
    (home-page "https://kyverno.io/")
    (license license:asl2.0)))

(define-public khelmfn
  (package
    (name "khelmfn")
    (version "2.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
	     "https://github.com/mgoltzsche/khelm/releases/download/v"
	     version "/khelm-linux-amd64"))
       (sha256 (base32 "1dbniwbqj8ayi7205ipy2xznfypqbgnim3mij95ai0lidwm5fjvz"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "khelm-linux-amd64" #o755))))
       #:substitutable? #f
       #:install-plan '(("khelm-linux-amd64" "/bin/khelmfn"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "khelm binary")
    (description "A Helm chart templating CLI, helm to kustomize converter, kpt function
and kustomize plugin.")
    (home-page "https://github.com/mgoltzsche/khelm")
    (license license:asl2.0)))

(define-public tnctl
  (package
    (name "tnctl")
    (version "0.3.31")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/appvia/terranetes-controller/releases/download/v" version "/tnctl-linux-amd64"))
       (sha256 (base32 "12csg1x787dvx0s50fjq89gmad094gr54jwvw017xm6psmxgy9yc"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (add-after 'unpack 'chmod
		    (lambda* (#:key #:allow-other-keys)
		      (chmod "tnctl-linux-amd64" #o755))))
       #:substitutable? #f
       #:install-plan '(("tnctl-linux-amd64" "/bin/tnctl"))))
    (supported-systems '("x86_64-linux"))
    (synopsis "tnctl binary")
    (description "CLI for the terraform controller which manages the life cycles of a
terraform resource, allowing developers to self-serve dependencies in
a controlled manner.")
    (home-page "https://github.com/appvia/terranetes-controller")
    (license license:gpl2)))
