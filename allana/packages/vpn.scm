(define-module (allana packages vpn)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages vpn)
  #:use-module (guix build utils)
  #:use-module (guix packages))

(define-public vpn-slice-modified
  (package
    (inherit vpn-slice)
    (name "vpn-slice-modified")
    (arguments
     '(#:phases
       (modify-phases %standard-phases
	 (add-after 'unpack 'set-iptables-path
	   (lambda _
	     (substitute* "vpn_slice/linux.py"
	       (("'/sbin/iptables'") (string-append "'" (which "iptables") "'")))
	     (substitute* "vpn_slice/linux.py"
	       (("'/sbin/ip'") (string-append "'" (which "ip") "'"))))))))
    (inputs
     (modify-inputs (package-inputs vpn-slice)
       (append iptables iproute)))))
