(define-module (allana packages ldap)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages openldap)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public openldap-libxcrypt
  (package
    (inherit openldap)
    (name "openldap-libxcrypt")
    (arguments
     (substitute-keyword-arguments (package-arguments openldap)
       ((#:phases phases)
        #~(modify-phases #$phases
            ;; slapd needs to be able to find libxcrypt
            (add-after 'install 'wrap-slapd
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (libxcrypt (assoc-ref inputs "libxcrypt")))
                  (wrap-program (string-append out "/libexec/slapd")
                    `("LD_LIBRARY_PATH" prefix
                      (,(format #f "~a/lib" libxcrypt)))))))))
       ((#:configure-flags configure-flags #~(list))
        #~(append '("--enable-crypt"     ;for libxcrypt
                    "--enable-debug"     ;logging
                    "--enable-dynlist"   ;dynamic lists
                    "--enable-refint"    ;referential integrity
                    "--enable-syncprov") ;sync replication
                 #$configure-flags))))
    (inputs
     (modify-inputs
      (package-inputs openldap)
      (delete libgcrypt)
      (append libxcrypt))))) ;replace libgcrypt with libxcrypt

(define-public python-ldap-libxcrypt
  (package
    (inherit python-ldap)
    (name "python-ldap-libxcrypt")
    (inputs
     (modify-inputs (package-inputs python-ldap)
       (replace "openldap" openldap-libxcrypt)))))
