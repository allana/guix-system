(define-module (allana packages certs)
  #:use-module (gnu)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages tls)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial))

(define-public geodata-certs
  (package
    (name "geodata-certs")
    (version "1")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((ca-gdo (assoc-ref %build-inputs "gdo.aws.pem"))
                         (ca-nib (assoc-ref %build-inputs "nib.aws.pem"))
                         (ca-dev (assoc-ref %build-inputs "dev.aws.pem"))
			 (ca-gdo-priv (assoc-ref %build-inputs "gdo.priv.pem"))
                         (out (string-append (assoc-ref %outputs "out")
                                             "/etc/ssl/certs"))
                         (openssl (assoc-ref %build-inputs "openssl"))
                         (perl (assoc-ref %build-inputs "perl")))
                     (mkdir-p out)
                     (for-each (lambda (cert)
                                 (copy-file cert
                                            (string-append out "/"
                                                           (strip-store-file-name
                                                            cert))))
                               (list ca-gdo ca-nib ca-dev ca-gdo-priv))

                     ;; Create hash symlinks suitable for OpenSSL ('SSL_CERT_DIR' and
                     ;; similar.)
                     (chdir (string-append %output "/etc/ssl/certs"))
                     (invoke (string-append perl "/bin/perl")
                             (string-append openssl "/bin/c_rehash") ".")))))
    (native-inputs (list openssl perl)) ;for 'c_rehash'
    (inputs `(("gdo.aws.pem" ,(local-file "gdo.aws.pem"))
              ("nib.aws.pem" ,(local-file "nib.aws.pem"))
              ("dev.aws.pem" ,(local-file "dev.aws.pem"))
	      ("gdo.priv.pem" ,(local-file "gdo.priv.pem"))))
    (home-page "")
    (synopsis "Geodata certificates")
    (description "Geodata certificates")
    (license #f)))
