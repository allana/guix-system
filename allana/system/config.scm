(define-module (allana system config)
  #:use-module (allana packages certs)
  #:use-module (gnu)
  #:use-module (gnu packages security-token)
  #:use-module (gnu services cups)
  #:use-module (gnu services desktop)
  #:use-module (gnu services dns)
  #:use-module (gnu services docker)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services syncthing)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services web)
  #:use-module (gnu services xorg)
  #:use-module (gnu system pam)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware sof-firmware))
  (locale "en_US.utf8")
  (timezone "Europe/Oslo")
  (keyboard-layout (keyboard-layout "no" "nodeadkeys"))
  (host-name "geodata.no")

  (users (cons* (user-account
                  (name "allan.adair")
                  (comment "Allan Adair")
                  (group "users")
                  (home-directory "/home/allan.adair")
                  (supplementary-groups '("docker" "wheel"
                                          "netdev"
                                          "audio"
                                          "video"
                                          "kvm"
                                          "plugdev"))) %base-user-accounts))

  (sudoers-file (plain-file "sudoers"
                            (string-append (plain-file-content
                                            %sudoers-specification)
                                           "%wheel ALL = NOPASSWD: ALL")))

  (packages (append (list geodata-certs
                          (specification->package "gvfs")) %base-packages))

  (services
   (append (list (service gnome-keyring-service-type)
                 (service gnome-desktop-service-type)
		 (service pam-limits-service-type
			  (list (pam-limits-entry "allan.adair" 'both 'nofile 100000)))
                 (udev-rules-service 'fido2 libfido2
                                     #:groups '("plugdev"))
                 (service whoogle-service-type)
                 (service syncthing-service-type
                          (syncthing-configuration (user "allan.adair")))
		 (service containerd-service-type)
                 (service docker-service-type
			  (docker-configuration (debug? #t)
						(config-file
						 (local-file "docker-config.json"))))
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout)))
                 (service qemu-binfmt-service-type
                          (qemu-binfmt-configuration (platforms (lookup-qemu-platforms
                                                                 "arm"
                                                                 "aarch64"))))
                 (simple-service 'resolv-service etc-service-type
                                 `(("resolv.conf" ,(local-file "resolv.conf"))))
		 (simple-service 'podman-subuid-subgid etc-service-type
				 `(("subuid"
				    ,(plain-file "subuid" "allan.adair:100000:65536"))
				   ("subgid"
				    ,(plain-file "subgid" "allan.adair:100000:65536"))))
                 (service cups-service-type)
                 (service dnsmasq-service-type
                          (dnsmasq-configuration (no-resolv? #t)
                                                 (query-servers-in-order? #t)
                                                 (servers '("/aws/10.15.7.4"
                                                            "/aws/10.16.1.4"
                                                            "/aws/10.16.1.5"
                                                            "/aws/172.16.0.26"
                                                            "/aws/172.16.0.25"
                                                            "/gdo.priv/10.15.7.4"
                                                            "/gdo.priv/10.16.1.4"
                                                            "/gdo.priv/10.16.1.5"
                                                            "/gdo.priv/172.16.0.26"
                                                            "/gdo.priv/172.16.0.25"
                                                            "1.1.1.1"
                                                            "1.0.0.1")))))
           (modify-services %desktop-services
             (guix-service-type config =>
                                (guix-configuration (inherit config)
                                                    (substitute-urls (append (list
                                                                              "https://substitutes.nonguix.org")
                                                                      %default-substitute-urls))
                                                    (authorized-keys (append (list
                                                                              (local-file
                                                                               "./signing-key.pub"))
                                                                      %default-authorized-guix-keys))))
             (gdm-service-type config =>
                               (gdm-configuration (inherit config)
                                                  (auto-login? #t)
                                                  (default-user "allan.adair")
                                                  (wayland? #t)))
             (network-manager-service-type config =>
                                           (network-manager-configuration (inherit
                                                                           config)
                                                                          (dns
                                                                           "none")))
             (upower-service-type config =>
                                  (upower-configuration (inherit config)
                                                        (ignore-lid? #t))))))

  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))

  (mapped-devices (list (mapped-device
                          (source (uuid "a21701b5-5ae4-4058-bc87-f03b92f59fd6"))
                          (target "cryptroot")
                          (type luks-device-mapping))))

  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "B2B9-31ED"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device "/dev/mapper/cryptroot")
                         (type "ext4")
                         (dependencies mapped-devices)) %base-file-systems)))
