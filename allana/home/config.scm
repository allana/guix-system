(define-module (allana home config)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services sound)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services ssh)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (guix channels)
  #:use-module (guix gexp)
  #:use-module (guix inferior)
  #:use-module (srfi srfi-1))

(home-environment
  (packages (map (compose list specification->package+output)
                 (list
                  ;; my packages
                  "argo"
                  "argocd"
                  "flux"
                  "helm"
                  "khelmfn"
                  "kops"
                  "kubectl"
                  "kustomize"
                  "kyverno"
                  "tee-clc"
                  "terraform"
                  "tnctl"
                  "vpn-slice-modified"
		  "diffoci"
		  "yamlfmt"

                  ;; yubikey
                  "libfido2"

                  ;; web
                  "google-chrome-stable"
                  "icecat"

                  ;; email
                  "evolution"

                  ;; emacs
                  "emacs"
                  "emacs-all-the-icons"
                  "emacs-all-the-icons-dired"
                  "emacs-envrc"
		  "emacs-transient"
                  "emacs-magit"
                  "emacs-treemacs"
                  "emacs-nov-el"
                  "emacs-geiser-guile"
                  "emacs-ef-themes"
                  "emacs-modus-themes"
                  "emacs-eglot"
                  "emacs-geiser"
		  "emacs-jsonrpc"
                  "emacs-org"
                  "emacs-org-roam"
                  "emacs-org-bullets"
                  "emacs-org-present"
                  "emacs-so-long"
                  "emacs-flycheck"
                  "emacs-undo-tree"
                  "emacs-editorconfig"
                  "emacs-which-key"
                  "emacs-rainbow-delimiters"
                  "emacs-graphviz-dot-mode"
                  "emacs-markdown-mode"
                  "emacs-yaml-mode"
                  "emacs-yasnippet"
                  "emacs-yasnippet-snippets"
                  "emacs-company"
                  "emacs-paredit"
                  "emacs-python-black"
                  "emacs-which-key"
                  "emacs-vterm"
                  "emacs-terraform-mode"
                  "emacs-dockerfile-mode"
                  "emacs-diminish"
                  "emacs-orderless"
                  "emacs-page-break-lines"
                  "emacs-plantuml-mode"
		  "emacs-apheleia"

                  ;; tree-sitter
                  "tree-sitter-scheme"
                  "tree-sitter-python"
                  "tree-sitter-json"
                  "tree-sitter-html"
                  "tree-sitter-bash"
                  "tree-sitter-plantuml"

                  ;; version control
                  "git"
                  "git:send-email"
                  "git-lfs"

                  ;; VPN
                  "openconnect"
                  "gp-saml-gui"

                  ;; guile
                  "guile"
                  "guile-colorized"
                  "guile-readline"
                  "guile-hall"
                  "haunt"
                  "nyacc"

                  ;; python
                  "python"
                  "python-wrapper"
                  "python-black"
                  "python-lsp-server"
                  "python-pydocstyle"
                  "python-yapf"
                  "python-mypy"
                  "python-flake8"
                  "python-jedi"
                  "python-isort"

                  ;; misc
                  "bind:utils"
		  "direnv"
                  "password-store"
                  "pass-git-helper"
                  "pinentry"
                  "plantuml"
		  "podman"
		  "podman-compose"
                  "recutils"
                  "seahorse"
		  "file-roller"
                  "vlc"
                  "flatpak"
                  "gimp"
                  "awscli"
                  "openssh"
                  "docker-cli"
                  "gnupg"
                  "dosfstools"
                  "curl"
                  "sicp"
                  "aspell"
                  "aspell-dict-en"
                  "jq"
                  "tree"
                  "unzip"
                  "sqlitebrowser"
                  "parallel"
                  "xdg-utils"
                  "xdg-desktop-portal-gtk")))

  (services
   (list (service home-files-service-type
                  `((".gnupg/gpg-agent.conf" ,(local-file "./gpg-agent.conf"))))
         (service home-bash-service-type
                  (home-bash-configuration
                   (environment-variables '(("ALTERNATE_EDITOR" . "")
                                            ("EDITOR" . "emacsclient")
                                            ("VISUAL" . "emacsclient -a emacs")
                                            ("TF_BYPASS_BROWSER_LOGIN" . "1")))
                   (aliases '(("grep" . "grep --color=auto")
			      ("ll" . "ls -l")
                              ("ls" . "ls -p --color=auto")
			      ("aws" . "docker run --rm -it -v ~/.aws:/root/.aws -v $(pwd):/aws amazon/aws-cli $@")))
                   (bashrc (list (local-file "./bashrc" "bashrc")
				 (local-file "./kubectl.completion.bash.inc"
					     "kubectl.completion.bash.inc")))
                   (bash-profile (list (local-file "./bash_profile"
                                                   "bash_profile")))))
	 (service home-xdg-configuration-files-service-type
		  `(("yamlfmt/.yamlfmt"
		     ,(plain-file
		       "yamlfmt"
		       "formatter:\n  indentless_arrays: true\n  retain_line_breaks: true\n"))
		    ("containers/containers.conf"
		     ,(plain-file
		       "containers.conf"
		       "[containers]\ndefault-ulimits = ['nofile=65535:65535']"))
		    ("containers/registries.conf"
		     ,(plain-file
		       "registries.conf"
		       "unqualified-search-registries = ['docker.io']"))
		    ("containers/storage.conf"
		     ,(plain-file
		       "storage.conf"
		       "[storage]\ndriver = \"overlay\""))
		    ("containers/policy.json"
		     ,(plain-file
		       "policy.json"
		       "{\"default\": [{\"type\": \"insecureAcceptAnything\"}]}"))))
         (service home-dbus-service-type)
         (service home-pipewire-service-type
                  (home-pipewire-configuration))
         (service home-openssh-service-type
                  (home-openssh-configuration
		   (hosts (list (openssh-host
				 (name "dev.azure.com")
                                 (user "git")
                                 (identity-file
                                  "/home/allan.adair/.ssh/id_rsa"))))
                   (known-hosts (list (local-file "known_hosts")))))
         (service home-ssh-agent-service-type
                  (home-ssh-agent-configuration)))))
